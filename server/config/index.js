const privateRoutes = require('../routes/api/privateRoutes');
const publicRoutes = require('../routes/api/publicRoutes');

const config = {
  migrate: true,
  privateRoutes,
  publicRoutes,
  port: process.env.PORT || '3000',
};

module.exports = config;
