export class ResponseBody{
    data;
    success;
    constructor(success = false, data = "INVALID_REQUEST"){
        this.data = data;
        this.success = success;
    }
    setData(data = false){
        this.data = data;
    }
    setSuccess(bool = true){
        this.success = bool;
    }

    getResponse(){
        return {
          success: this.success,
          data: this.data
        };
    }
}