const User = require('../models/User');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const nodemailer = require("nodemailer");

import {ResponseBody} from './Response';

const UserController = () => {
  ////////////////need to implement ////////////////////////////
  const register = async (req, res) => {
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    secure: false, 
    auth: {
      user: 'ramchandra.bluehorse@gmail.com', // generated ethereal user
      pass: 'ramkumer@1995', // generated ethereal password
    },
  });

    
    let respBody = new ResponseBody(false);
    const { body } = req;
    const {password, email, name,user_type,address,postal_code,city,state,country} = body;

    var mailOptions = {
      from: "ramchandra.bluehorse@gmail.com",
      to: req.body.email,
      subject: "new register successfull ",
      text:
        "please check your email...",};


    if(name && email && password && user_type && address&& postal_code &&city&& state&&country){
      try {
        const user = await User.create({
          email, password, name,user_type,city,country,address,postal_code,state
        });
        const token = authService().issue({ id: user.id });
        respBody.setData({
          token, user
        });

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
        });
        // let info = await transporter.sendMail({
        //   from: 'ramchandra.bluehorse@gmail.com', // sender address
        //   to: "bar@example.com, baz@example.com", // list of receivers
        //   subject: "Hello ✔", // Subject line
        //   text: "Hello world?", // plain text body
        //   html: "<b>Hello world?</b>", // html body
        // });
        console.log(token);
        respBody.setSuccess();
        return res.status(200).json({"message":'Registration successfull.please check your Email..'});
      } catch (err) {
        console.log(err);
        respBody.setData('INTERNAL_ERROR');
        return res.status(500).json(respBody.getResponse());
      }
    }

    return res.status(400).json(respBody.getResponse());
  };

  const login = async (req, res) => {
    let respBody = new ResponseBody(false);
    const { email, password,user_type } = req.body;

    if (email && password && user_type) {
      try {
        const user = await User
          .findOne({
            where: {
              email,
              user_type
            },
          });

        if (!user) {
          respBody.setData('INVALID_USER');
          return res.status(400).json(respBody.getResponse());
        }

        if (bcryptService().comparePassword(password, user.password)) {
          const token = authService().issue({ id: user.id });
          respBody.setData({
            token, user
          });
          respBody.setSuccess();
          return res.status(200).json(respBody.getResponse());
        }
        respBody.setData('UNAUTHORIZED');
        return res.status(401).json(respBody.getResponse());
      } catch (err) {
        console.log(err);
        respBody.setData('INTERNAL_ERROR');
        return res.status(500).json(respBody.getResponse());
      }
    }
    respBody.setData('BAD_REQUEST');
    return res.status(400).json(respBody.getResponse());
  };

/////// to do need for implement  /////////////
  const updateProfile =  (req, res) => {
    let respBody = new ResponseBody(false);
     let user_Id = req.body.id;
    const { token } = req.body;
    authService().verify(token,(err)=>{
      if(err){
        respBody.setData("INVALID_TOKEN");
        respBody.setSuccess(false);
        return res.status(401).json(respBody.getResponse())
      }else{
        let id1 = User.findOne({where:{id:user_Id}});
        if(!id1){
          return res.status(200).json({'message':'User id not found'})
        }else{
          let pass = req.body.password;
          let hashPassword = bcryptService().password(pass);
           const user = User.update({
              email:req.body.email,
              name:req.body.name,
              user_type:req.body.user_type,
              password:hashPassword,
              address:req.body.address,
              postal_code:req.body.postal_code,
              state:req.body.state,
              country:req.body.country,
              city:req.body.city,
            })
            // authService().issue({ id: user.id });
            // respBody.setData({
            //   token, user
            // });
            respBody.setSuccess();
            return res.status(200).json({data:user});
        }
     
      }
    })
  };
////////////////////////End ////////////////////////////////////
  const validate = (req, res) => {
    let respBody = new ResponseBody(false);
    const { token } = req.body;

    authService().verify(token, (err) => {
      if (err) {
        respBody.setData("INVALID_TOKEN");
        respBody.setSuccess(false);
        return res.status(401).json(respBody.getResponse());
      }
      respBody.setData("VALID_TOKEN");
      respBody.setSuccess();
      return res.status(200).json(respBody.getResponse());
    });
    return res.status(400).json(respBody.getResponse());
  };

  const getAll = async (req, res) => {
    let respBody = new ResponseBody(false);
    try {
      const users = await User.findAll();
      respBody.setData(users);
      respBody.setSuccess();
      return res.status(200).json(respBody.getResponse());
    } catch (err) {
      console.log(err);
      respBody.setData('UNAUTHORIZED');
      return res.status(500).json(respBody.getResponse());
    }
  };


  return {
    register,
    login,
    validate,
    getAll,
    updateProfile
  };
};

module.exports = UserController;
