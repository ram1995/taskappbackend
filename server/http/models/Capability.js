const Sequelize = require('sequelize')
const sequelize = require('../../config/database');

const tableName = 'capability';
const Capability = sequelize.define('Capability',{
    id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    user_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    project_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    status:{
        type:Sequelize.ENUM('active','inactive'),
        allowNull:true
    },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
},{tableName});

module.exports = Capability;