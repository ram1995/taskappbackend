const Sequelize = require('sequelize')
const sequelize = require('../../config/database');

const tableName = 'issue_updates';
const Issue_Update = sequelize.define('Issue_Update',{
    id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    user_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    issue_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    project_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    tracker_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    IssueSummary:{
        type:Sequelize.STRING(255),
        allowNull:true
    },
    IssueAttachFile:{
        type:Sequelize.TEXT(),
        allowNull:true
    },
    requireTime:{
        type:Sequelize.STRING(255),
        allowNull:true
    },
   onDate:{
        type:Sequelize.STRING(255),
        allowNull:true
   },
    status:{
        type:Sequelize.ENUM('approved','declined'),
        allowNull:true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
    },
    updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
    }
},{tableName});

module.exports=Issue_Update;