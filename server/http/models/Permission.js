const Sequelize = require('sequelize')
const sequelize = require('../../config/database');

const tableName = 'permission';
const Permission = sequelize.define('Permission',{
    id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    user_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    role_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    issue_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    status:{
        type:Sequelize.ENUM('accepted','decliend'),
        allowNull:true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false
    },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
},{tableName});

module.exports = Permission;