const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const tableName = 'projects';
const Project = sequelize.define('Project', {
      id: {
        type: Sequelize.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      slug: {
        type: Sequelize.STRING(64),
        allowNull: true
      },
      title: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      short_description: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      status:{
        type:Sequelize.ENUM('active','inactive'),
        allowNull:true
    },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedBy: {
        type: Sequelize.STRING(128),
        allowNull: true
      }
    }, {
      tableName
    });


  module.exports = Project;