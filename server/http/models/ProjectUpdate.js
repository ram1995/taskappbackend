const Sequelize = require('sequelize')
const sequelize = require('../../config/database');

const tableName = 'project_updates';
const Project_Update = sequelize.define('Project_Update',{
    id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    user_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    issue_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    project_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    tracker_id:{
        type:Sequelize.INTEGER(10).UNSIGNED,
        allowNull:true
    },
    ProjectSummary:{
        type:Sequelize.STRING(255),
        allowNull:true
    },
    ProjectAttachFile:{
        type:Sequelize.TEXT(),
        allowNull:true
    },
    requireTime:{
        type:Sequelize.STRING(255),
        allowNull:true
    },
   onDate:{
        type:Sequelize.STRING(255),
        allowNull:true
   },
    status:{
        type:Sequelize.ENUM('approved','declined'),
        allowNull:true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
    },
    updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
    }
},{tableName});

module.exports = Project_Update;