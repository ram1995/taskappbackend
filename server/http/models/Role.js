const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const tableName = 'roles';
const Role = sequelize.define('Role',{
    id: {
        type: Sequelize.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      slug: {
        type: Sequelize.STRING(64),
        allowNull: true
      },
      name:{
          type:Sequelize.STRING,
          allowNull:true
      },
     status:{
         type:Sequelize.ENUM('active','inactive'),
         allowNull:true
     },
     createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
},{tableName});


module.exports = Role;
