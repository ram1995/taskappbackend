const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
};

const tableName = 'users';

const User = sequelize.define('User', {
  user_type:{
      type:Sequelize.STRING,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
  },
  isVerified: {
     type:Sequelize.BOOLEAN,
      defaultValue:false
    },
  name: {
    type: Sequelize.STRING,
  },
  address:{
    type: Sequelize.STRING,
  },
  postal_code:{
    type:Sequelize.INTEGER,
  },
  city:{
    type:Sequelize.STRING,
  },
  state:{
    type:Sequelize.STRING,
  },
  country:{
    type:Sequelize.STRING,
  }

}, {hooks, tableName });

// eslint-disable-next-line
User.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  delete values.password;
  return values;
};

module.exports = User;
