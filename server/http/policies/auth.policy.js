import {ResponseBody} from "../controllers/Response";

const JWTService = require('../services/auth.service');

// usually: "Authorization: Bearer [token]" or "token: [token]"
module.exports = (req, res, next) => {
  let tokenToVerify;
  let respBody = new ResponseBody(false);

  if (req.header('Authorization')) {

    const parts = req.header('Authorization').split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/.test(scheme)) {
        tokenToVerify = credentials;
      } else {
        respBody.setData('INVALID_TOKEN_SIGNATURE');
        return res.status(401).json(respBody.getResponse());
      }
    } else {
      respBody.setData('INVALID_TOKEN_SIGNATURE');
      return res.status(401).json(respBody.getResponse());
    }
  } else if (req.body.token) {
    tokenToVerify = req.body.token;
    delete req.query.token;
  } else {
    respBody.setData('NO_TOKEN_FOUND');
    return res.status(401).json(respBody.getResponse());
  }

  return JWTService().verify(tokenToVerify, (err, thisToken) => {
    if (err) return res.status(401).json({ err });
    req.token = thisToken;
    return next();
  });
};
